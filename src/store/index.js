import { createStore } from 'vuex'
import axios from 'axios'

export default createStore({
  state: {
      name:'',   
      contact:null,
      selectedCountry:'',
      selectedGender:'',
      selectedState:'',
      country:[
        {name:'India'},
        {name:'America'},
        {name:'London' },
        {name:'Africa' }
      ],
        selectedGender:null,
      gender:[
        {name:'Male'  },
        {name:'Female'},
        {name:'other' }
      ],
       selectedState:null,
      cstate:[
        {name:'TamilNadu'   },
        {name:"Andhra Pradesh"},
        {name:'Sikkim'},
        {name:'Meghalaya'},
        {name:'Assam'},
        {name:'Gujarat'},
        {name:'Madya Pradesh'},
        {name:'Maharashtra'},
        {name:'Kerala'}
      
      ],
      
      // companyName:'',
      // email:'',
      // job:"",
      // experience:"",
      // checked:"",

      // EmailVerification
      inputNumber1:null,
      inputNumber2:null,
      inputNumber3:null,
      inputNumber4:null,
      inputNumber5:null,
      
  },
  mutations:{
     
  },
  getters: {
   
  },
 
  actions: {
    async addPerson({commit},{name,selectedGender,selectedCountry,selectedState,contact}){
      const response = await axios.post("http://localhost:5600/person",{name,selectedGender,selectedCountry,selectedState,contact})
      console.log(response.data)

      commit('newPerson', response.data)
    },

    async addCompany({commit},{companyName,email,job,experience,checked}){
      const response = await axios.post("http://localhost:5600/company",{companyName,email,job,experience,checked})
      console.log(response.data)

      commit('newCompany', response.data)
    },
    async addEmail({commit},{inputNumber1,inputNumber2,inputNumber3,inputNumber4,inputNumber5}){
      const response = await axios.post("http://localhost:5600/email",{inputNumber1,inputNumber2,inputNumber3,inputNumber4,inputNumber5})
      console.log(response.data)

      commit('newCompany', response.data)
    }
  },


  modules: {
  }
})
